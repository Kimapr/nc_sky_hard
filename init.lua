-- Copyright 2020 (C) "Kimapr" Dmitriy Tillyaev ()
-- This work is licensed under MIT license

local borrad = 8
local genrad = 8
local ynoise = 4
local hmin = 64
local hmax = 128
local _ = {name = "air"}
local D = {name = "nc_terrain:dirt"}
local G = {name = "nc_terrain:dirt_with_grass"}
local S = {name = "nc_terrain:stone"}
local T = {name = "nc_tree:eggcorn_planted"}

local schem = {
   size = {x=5,y=5,z=5},
   data = {
     _,_,_,_,_,
     _,_,_,_,_,
     _,G,G,G,_,
     _,_,_,_,_,
     _,_,_,_,_,

     _,_,_,_,_,
     _,D,D,D,_,
     G,G,G,G,G,
     _,_,_,_,_,
     _,_,_,_,_,

     _,_,S,_,_,
     _,D,S,D,_,
     G,G,T,G,G,
     _,_,_,_,_,
     _,_,_,_,_,
     
     _,_,_,_,_,
     _,D,D,D,_,
     G,G,G,G,G,
     _,_,_,_,_,
     _,_,_,_,_,
     
     _,_,_,_,_,
     _,_,_,_,_,
     _,G,G,G,_,
     _,_,_,_,_,
     _,_,_,_,_,
   }
}

local function island(pos,call,...)
  local r = 8
  local x,y,z = pos.x,pos.y,pos.z
  local args = {...}
  minetest.emerge_area({x=x-r,y=y-r,z=z-r},{x=x+r,y=y+r,z=z+r},function(bp,act,crem)
    if crem > 0 then
      return
    end
    minetest.place_schematic(pos,schem,"0",{},false,"place_center_x, place_center_y, place_center_z")
    nodecore.soaking_abm_push(pos, "eggcorn",100000)
    call(pos,unpack(args))
  end)
end

nodecore.override_sky_islandgen(island,borrad,genrad,{x=0,y = 666 * 0 + (2^666+666+666+666+666) / math.huge + 13.00666, z=0},ynoise,hmin,hmax)


-- ~ HAVE A  B_A_D  T_I_M_E ~
-- [megalovania intensifies]
